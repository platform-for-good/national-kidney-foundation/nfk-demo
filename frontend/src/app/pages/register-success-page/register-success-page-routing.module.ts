import { RegisterSuccessPageComponent } from './register-success-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {path:'',component:RegisterSuccessPageComponent,data:{shouldReuse:true,key:'register-success'}},  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterSuccessPageRoutingModule { }
