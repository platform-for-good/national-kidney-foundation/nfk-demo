import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss']
})
export class ConfirmationPageComponent implements OnInit {

  APP = 'assets/pf8-2.png';
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
  errorMessage: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.errorMessage = '';
    if (this.authService.isLogged()) {
      this.navigateTo();
    }
  }

  public async confirm() {
    if (this.newPassword !== this.confirmPassword) {
      this.errorMessage = 'Passwords do not match.';

    } else {
      try {
        let url;
        url = (await this.authService.login(
          this.authService.userName,
          this.currentPassword,
          this.newPassword,
        )) as string;

        this.navigateTo(url);

      } catch (e) {
        this.errorMessage = 'Wrong Credentials!';
      }
    }
  }

  public navigateTo(url?: string) {
    url = url || 'nav';
    this.router.navigate([url], { replaceUrl: true });
  }
}
