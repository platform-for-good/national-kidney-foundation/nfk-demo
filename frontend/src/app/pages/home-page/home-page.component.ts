import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {

    LOGO = 'assets/logo.png';
    // PROJECTS = 'assets/projects.jpg';
    // SPONSOR = 'assets/Sponsor.jpg';
    // TEAMMEMBERS = 'assets/teammembers.jpg';

    data: {
        title: string,
        icon: any,
        content: string,
        instruction: string
    }[]

    constructor() { }

    ngOnInit() {
        this.data = [];
       
        this.data.push({
            title: 'NKF',
            icon: this.LOGO,
            content: 'Donate to help!',
            instruction: ''
        });
    }
}
