import { Injectable } from '@angular/core';
import { CrudService } from '../core/services/http/crud.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../core/services/storage/storage.service';
import { StorageKey } from '../core/services/storage/storage.model';
const { AUTH_TOKEN, ACCESS_TOKEN, CREATE_NEW_USER, CURRENT_USER } = StorageKey;

@Injectable({
    providedIn: 'root',
})
export class AuthService extends CrudService {
    endpoint = 'auth';
    redirectUrl: string;
    token: string;
    userName: string;

    constructor(http: HttpClient, private storage: StorageService) {
        super(http);
        this.token = this.storage.read(AUTH_TOKEN) || '';
        this.url = 'https://t486s92c6f.execute-api.us-east-1.amazonaws.com/dev';
    }

    public async mockLogin(email: string, password: string) {
        try {
            if (!(email === 'user' && password === 'user')) {
                throw new Error(
                    'When using mockLogin, login with credentials: \nemail: user\npassword:user',
                );
            }

            this.token = 'user';
            this.storage.save(StorageKey.AUTH_TOKEN, this.token);

            return this.redirectUrl;

        } catch (e) {
            return Promise.reject(e.message);
        }
    }

    public getToken(): string {
        return this.token;
    }

    public async login(email: string, password: string, newPassword?: string) {

        // https://t486s92c6f.execute-api.us-east-1.amazonaws.com/dev/tenant_login
        this.endpoint = 'tenant_login';
        try {

            this.userName = email;

            let payload = {
                userName: this.userName,
                password: password
            }

            let createNewUser = false;
            if (newPassword) {
                payload["newPassword"] = newPassword;
                createNewUser = true;
            }

            const response = await this.post(payload);
            if (response.newPasswordRequired) {
                this.redirectUrl = 'confirmation';

            } else {

                this.redirectUrl = 'nav';
                // {
                //     "token": "eyJraWQiOiJFMTBvTkdMOWN5NnI4RGxcL0V2amVCa3FLXC82alJFb2M3NGZPWkNCWmxxaUU9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiI5MTk5MjMxNS0wMjNkLTQxYWQtYjNhMy0zYmUwODlkNTgzNWMiLCJjdXN0b206dGllciI6IkZyZWUgVGllciIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX0xNOTQ3U1Q3QyIsImNvZ25pdG86dXNlcm5hbWUiOiJuZWF2ZW43N0BnbWFpbC5jb20iLCJjdXN0b206dGVuYW50X2lkIjoiVEVOQU5UMGJjMjU5ZjM1YzYyNGIyYjkwZWVkY2QxN2I0YzMwMGUiLCJnaXZlbl9uYW1lIjoiWW9uZyBLaGVuZyIsImN1c3RvbTpnb29nbGVfYXBpIjoiYWRhZmFmIiwiYXVkIjoiZmRkaXIxc3JubnAzbTRkMGY3amliNG40cCIsImV2ZW50X2lkIjoiMDhmYWVkMzEtN2FkNi00Y2U1LWJiNTctMjEyNzRjN2MwOTU3IiwiY3VzdG9tOmZiX2FwaSI6ImFkYWZhZiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjAyNjczNzgyLCJleHAiOjE2MDI2NzczODIsImN1c3RvbTpyb2xlIjoiVGVuYW50QWRtaW4iLCJpYXQiOjE2MDI2NzM3ODIsImZhbWlseV9uYW1lIjoiU2VvIiwiZW1haWwiOiJuZWF2ZW43N0BnbWFpbC5jb20ifQ.gyQ6nk9hGz92mK9ngun_rzoll5aMmOO9zGpQYUuXM-tgNMEC6gcoswNqPiZJ0eF9Xk-P7PamJLMkg22eYchHsqp-Wd09ROPpyuXXwtV3pg658f_tsj__I65qJWl8URuTSpoG7qSW8nQMNucVXQj9LoyGLAYSLlkWImbKIELoeuezNvOBgiaK27X0p53rOcOUmnF_yzzb-lxHk7QV-N2brdmgjpF8IbVsZksAAq8-Afe_4WT_EVSdgh8ZKD-sbNEjtwQWISIEERDXhLZHlC9Upq8zoqBj5oq44pYYlMfMvpbAmYK8kaZsUndr59NhQVVVhBvutvx5CpZgoMrRa4lXiw",
                //     "access": "eyJraWQiOiJ5WlhhcE1qR3IrcVB2OVFQaTRrQUE5K0cyZFdTNlp0THBsSnROM3Q4WlZBPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI5MTk5MjMxNS0wMjNkLTQxYWQtYjNhMy0zYmUwODlkNTgzNWMiLCJldmVudF9pZCI6IjA4ZmFlZDMxLTdhZDYtNGNlNS1iYjU3LTIxMjc0YzdjMDk1NyIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiYXdzLmNvZ25pdG8uc2lnbmluLnVzZXIuYWRtaW4iLCJhdXRoX3RpbWUiOjE2MDI2NzM3ODIsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX0xNOTQ3U1Q3QyIsImV4cCI6MTYwMjY3NzM4MiwiaWF0IjoxNjAyNjczNzgyLCJqdGkiOiIwZGU2ZDNmMi05Y2IxLTRmZDYtYWUwOS1kNDI5MjFmODJhNTMiLCJjbGllbnRfaWQiOiJmZGRpcjFzcm5ucDNtNGQwZjdqaWI0bjRwIiwidXNlcm5hbWUiOiJuZWF2ZW43N0BnbWFpbC5jb20ifQ.SqMVacw9sFRRtYsBkcMWdBVLriKnMBteJPc1-FYWI-p7mNNTgcZ7t1SFJkJMgqIfApLDA7xT-BYKZZbixwfD5zNqxxr7Qq25x7Xh29Y55_pPJDqgNh2s7mIXEFGR1G73ZJXKKeq_qbRi6pw_eAVAPYlFJFjOe2oIsAr-2FdEOVY551JMEwHrUe_GrATDMkgl4xS-_-IW-df6esTr3L4d5TjLa8aFygjYM8_GOENoMhnnNM0awzsTLUl0Q-HITZmeGXFzGEif6wY-OIAKp92c21QPTgdSdPihDTD5rfg1oaMbdVusOKRvTaGnDt4ZUmPW8KYk0xRNw_dkqfmtvIc6_g"
                // }
                this.token = response.token;
                this.assignAccessToken(response.access);
                this.storage.save(AUTH_TOKEN, response.token);
                this.storage.save(ACCESS_TOKEN, response.access);
            }

            if (createNewUser) {
                // this.redirectUrl = 'nav/profile';
                this.storage.save(CREATE_NEW_USER, true);
            }

            return this.redirectUrl;

        } catch (error) {
            return Promise.reject(error);
        }
    }

    public logout() {
        this.token = '';
        this.storage.remove(AUTH_TOKEN);
        this.storage.remove(ACCESS_TOKEN);
        this.storage.remove(CREATE_NEW_USER);
        this.storage.remove(CURRENT_USER);
    }

    public isLogged(): boolean {
        return this.token.length > 0;
    }

    public async mockRegister(username: string, email: string) {
        try {
            if (!(email === 'user' && username === 'user')) {
                throw new Error(
                    'When using mockRegister, register with credentials: \nusername: user\nemail:user',
                );
            }

            this.redirectUrl = 'register-success';

            return this.redirectUrl;

        } catch (e) {
            return Promise.reject(e.message);
        }
    }

    public async register(firstName: string, lastName: string, email: string) {

        // https://t486s92c6f.execute-api.us-east-1.amazonaws.com/dev/register_tenant
        this.endpoint = 'register_tenant';
        try {

            const payload = {
                id: 'nkf-TENANT',
                companyName: 'NKF Demo',
                accountName: email.substring(0, email.indexOf('@')),
                ownerName: email,
                tier: 'Free Tier',
                email: email,
                userName: email,
                firstName: firstName,
                lastName: lastName
            }
            
            const response = await this.post(payload);
            // sample payload
            /*
            "resp": {
                "id": "p4gTENANT",
                "companyName": "The company name",
                "accountName": "neaven77",
                "ownerName": "neaven77@gmail.com",
                "tier": "Free Tier",
                "email": "neaven77@gmail.com",
                "userName": "neaven77@gmail.com",
                "firstName": "Yong Kheng",
                "lastName": "Seo",
                "UserPoolId": "us-east-1_NJ6Nt0HFq",
                "IdentityPoolId": "us-east-1:1f60ed99-ba42-49a7-95dc-abd29b99806d"
            }
            //*///

            this.redirectUrl = 'register-success';
            return this.redirectUrl;

        } catch (e) {
            return Promise.reject(e.message);
        }
    }
}
